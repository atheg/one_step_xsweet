# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
Gem::Specification.new do |spec|
  spec.name           = 'one_step_xsweet'
  spec.version        = '1.0.0'
  spec.date           = '2017-12-07'
  spec.summary        = "One step xsweet"
  spec.description    = "Runs all xsl sheets in one step"
  spec.authors        = ["Alex Theg"]
  spec.email          = 'alex@coko.foundation'
  spec.homepage       = 'https://gitlab.coko.foundation/alex/one_step_xsweet'
  spec.license        = 'MIT'
  spec.executables    = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files     = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths  = %w(lib)
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "webmock"
  spec.add_development_dependency "httparty"
  spec.required_ruby_version = '~> 2.2'
  spec.files         = Dir.glob("{lib}/**/*") + %w(./README.md)
end
