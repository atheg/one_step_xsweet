require 'coko_conversion/ink_step/coko/one_step_xsweet/download_and_execute_xsl_via_saxon'

module InkStep::Coko
  class OneStepXsweet < XsweetPipeline::DownloadAndExecuteXslViaSaxon

    def perform_step
      source_file_relative_path = find_source_file(regex: parameter(:regex))
      source_file_path = File.join(working_directory, source_file_relative_path)

      # EXTRACT

      log_as_step "extract"
      download_file(parameter(:extract_xsl_uri))
      apply_xslt_transformation(input_file_path: source_file_path,
                                output_file_path: extract_done_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # NOTES

      log_as_step "extract notes"
      download_file(parameter(:extract_notes_xsl_uri))
      apply_xslt_transformation(input_file_path: extract_done_path,
                                output_file_path: extract_notes_done_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # SCRUB

      log_as_step "scrub"
      download_file(parameter(:scrub_xsl_uri))
      apply_xslt_transformation(input_file_path: extract_notes_done_path,
                                output_file_path: scrub_done_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # LINKS

      log_as_step "links"
      download_file(parameter(:links_xsl_uri))
      apply_xslt_transformation(input_file_path: scrub_done_path,
                                output_file_path: links_done_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # JOIN

      log_as_step "join"
      download_file(parameter(:join_xsl_uri))
      apply_xslt_transformation(input_file_path: links_done_path,
                                output_file_path: join_done_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # COLLAPSE

      log_as_step "collapse"
      download_file(parameter(:collapse_xsl_uri))
      apply_xslt_transformation(input_file_path: join_done_path,
                                output_file_path: collapse_done_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # PROMOTELISTS

      log_as_step "marking lists"
      download_file(parameter(:mark_lists_xsl_uri))
      apply_xslt_transformation(input_file_path: collapse_done_path,
                                output_file_path: lists_marked_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      log_as_step "itemizing lists"
      download_file(parameter(:itemize_lists_xsl_uri))
      apply_xslt_transformation(input_file_path: lists_marked_path,
                                output_file_path: lists_itemized_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # cleanup after lists
      delete_file(lists_marked_path)

      # FINALRINSE

      log_as_step "rinsing"
      download_file(parameter(:rinse_xsl_uri))
      apply_xslt_transformation(input_file_path: lists_itemized_path,
                                output_file_path: rinsed_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # SPLITONB

      log_as_step "splitting ps on breaks in #{source_file_relative_path} to make #{p_split_on_br_done_path}..."
      download_file(parameter(:p_split_on_br_uri))
      apply_xslt_transformation(input_file_path: rinsed_path,
                                output_file_path: p_split_on_br_done_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # DIGESTPARA

      log_as_step "digesting paragraphs on #{source_file_relative_path} to make #{paragraphs_digested_path}..."
      download_file(parameter(:digest_paragraphs_xsl_uri))
      apply_xslt_transformation(input_file_path: p_split_on_br_done_path,
                                output_file_path: paragraphs_digested_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # MAKEHEADERXSL

      log_as_step "creating header escalator xslt (#{header_escalator_xslt_path}..."
      download_file(parameter(:make_header_escalator_xslt_uri))
      apply_xslt_transformation(input_file_path: paragraphs_digested_path,
                                output_file_path: header_escalator_xslt_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # APPLYHPXSL

      log_as_step "applying header escalator xslt on #{source_file_relative_path}..."
      apply_xslt_transformation(input_file_path: p_split_on_br_done_path,
                                output_file_path: headers_promoted_path,
                                xsl_file_path: headers_escalated_xslt_path,
                                provided_saxon_jar_path: nil)

      # cleanup after hp

      delete_file(paragraphs_digested_path)

      # EDITORIANOTES

      log_as_step "reformatting notes on #{p_split_on_br_done_path} to make #{notes_done_path}..."
      download_file(parameter(:editoria_notes_xsl_uri))
      apply_xslt_transformation(input_file_path: headers_promoted_path,
                                output_file_path: notes_done_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # EDITORIABASIC

      log_as_step " (#{basic_done_path}..."
      download_file(parameter(:editoria_basic_xsl_uri))
      apply_xslt_transformation(input_file_path: notes_done_path,
                                output_file_path: basic_done_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)

      # EDITORIAREDUCE

      log_as_step "applying editoria reduce xslt on #{basic_done_path}..."
      download_file(parameter(:editoria_reduce_xsl_uri))
      apply_xslt_transformation(input_file_path: basic_done_path,
                                output_file_path: source_file_path,
                                xsl_file_path: xsl_file_path,
                                provided_saxon_jar_path: nil)
      success!
    end

    def self.description
      "XSweet one step"
    end

    def self.human_readable_name
      "Xsweet one step"
    end

    def required_parameters
      # e.g. [:foo, :bar]
      [
          :extract_xsl_uri,
          :extract_notes_xsl_uri,
          :scrub_xsl_uri,
          :links_xsl_uri,
          :join_xsl_uri,
          :collapse_xsl_uri,
          :mark_lists_xsl_uri,
          :itemize_lists_xsl_uri,
          :rinse_xsl_uri,
          :p_split_on_br_uri,
          :make_header_escalator_xslt_uri,
          :digest_paragraphs_xsl_uri,
          :editoria_notes_xsl_uri,
          :editoria_basic_xsl_uri,
          :editoria_reduce_xsl_uri
      ]
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {
          extract_xsl_uri: "Location of raw XSL file to download - extraction",
          extract_notes_xsl_uri: "Location of raw XSL file to download - notes extraction",
          scrub_xsl_uri: "Location of raw XSL file to download - scrub",
          links_xsl_uri: "Location of raw XSL file to download - links",
          join_xsl_uri: "Location of raw XSL file to download - join",
          collapse_xsl_uri: "Location of raw XSL file to download - collapse",
          mark_lists_xsl_uri: "Location of raw XSL file to download - mark lists",
          itemize_lists_xsl_uri: "Location of raw XSL file to download - itemize lists",
          rinse_xsl_uri: "Location of raw XSL file to download - rinse",
          p_split_on_br_uri: "Location of raw XSL file to download - splits <br> as <p>",
          digest_paragraphs_xsl_uri: "Location of raw XSL file to download - paragraph digester",
          make_header_escalator_xslt_uri: "Location of raw XSL file to download - header escalator",
          editoria_notes_xsl_uri: "Location of raw XSL file to download - handles editoria notes",
          editoria_basic_xsl_uri: "Location of raw XSL file to download - basic HTML replacements",
          editoria_reduce_xsl_uri: "Location of raw XSL file to download - reducer",
          regex: "Regular expression for input file"
      }
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {
          extract_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/docx-extract/docx-html-extract.xsl",
          extract_notes_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/docx-extract/handle-notes.xsl",
          scrub_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/docx-extract/scrub.xsl",
          links_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/local-fixup/hyperlink-inferencer.xsl",
          join_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/docx-extract/join-elements.xsl",
          collapse_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/docx-extract/collapse-paragraphs.xsl",
          mark_lists_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/list-promote/mark-lists.xsl",
          itemize_lists_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/list-promote/itemize-lists.xsl",
          rinse_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/html-polish/final-rinse.xsl",
          p_split_on_br_uri: "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/p-split-around-br.xsl",
          digest_paragraphs_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/header-promote/digest-paragraphs.xsl",
          make_header_escalator_xslt_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/header-promote/make-header-escalator-xslt.xsl",
          editoria_notes_xsl_uri: "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/editoria-notes.xsl",
          editoria_basic_xsl_uri: "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/editoria-basic.xsl",
          editoria_reduce_xsl_uri: "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/editoria-reduce.xsl",
          regex: [/\.html$/, /\.htm$/]
      }
    end

    def extract_done_path
      File.join(working_directory, "extracted.html")
    end

    def extract_notes_done_path
      File.join(working_directory, "notes_extracted.html")
    end

    def scrub_done_path
      File.join(working_directory, "scrubbed.html")
    end

    def links_done_path
      File.join(working_directory, "links.html")
    end

    def join_done_path
      File.join(working_directory, "joined.html")
    end

    def collapse_done_path
      File.join(working_directory, "collapsed.html")
    end

    def lists_marked_path
      File.join(working_directory, "lists_marked.html")
    end

    def lists_itemized_path
      File.join(working_directory, "lists_itemized.html")
    end

    def rinsed_path
      File.join(working_directory, "rinsed.html")
    end

    def p_split_on_br_done_path
      File.join(working_directory, "p_split_on_br_done.html")
    end

    def paragraphs_digested_path
      File.join(working_directory, "paragraphs_digested.html")
    end

    def header_escalator_xslt_path
      File.join(working_directory, "header_escalator.xslt")
    end

    def headers_promoted_path
      File.join(working_directory, "header_escalator.xslt")
    end

    def notes_done_path
      File.join(working_directory, "notes_done.html")
    end

    def basic_done_path
      File.join(working_directory, "basic_done.html")
    end

  end
end
