module CokoConversion
  if defined?(Rails)
    require 'coko_conversion/engine'
  else
    require 'coko_conversion/ink_step/coko/one_step_xsweet/download_and_execute_xsl_via_saxon'
    require 'coko_conversion/ink_step/coko/one_step_xsweet/one_step_xsweet'
  end
end
